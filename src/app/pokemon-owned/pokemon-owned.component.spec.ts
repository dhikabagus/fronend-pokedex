import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonOwnedComponent } from './pokemon-owned.component';

describe('PokemonOwnedComponent', () => {
  let component: PokemonOwnedComponent;
  let fixture: ComponentFixture<PokemonOwnedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonOwnedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemonOwnedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
