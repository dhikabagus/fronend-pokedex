import { Component, OnInit, ɵɵqueryRefresh } from '@angular/core';
import { PokemonApiService } from '../pokemon-api.service';

@Component({
  selector: 'app-pokemon-owned',
  templateUrl: './pokemon-owned.component.html',
  styleUrls: ['./pokemon-owned.component.css']
})
export class PokemonOwnedComponent implements OnInit {

  constructor(private service: PokemonApiService) { }
  pokemonDatas: any;

  ngOnInit(): void {
    this.service.getAllOwnedData().subscribe((res) => {
      this.pokemonDatas = res.data;
    })
  }
  refresh(): void {
    window.location.reload();
  }
  deletePokemon(pokemonId: any) {
    this.service.deleteData(pokemonId).subscribe()
    this.refresh()
  }
}
