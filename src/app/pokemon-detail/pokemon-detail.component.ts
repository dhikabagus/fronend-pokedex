import { Component, OnInit } from '@angular/core';
import { PokemonApiService } from '../pokemon-api.service';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  constructor(private service: PokemonApiService, private router:ActivatedRoute) { }

  getParamId:any;
  successMsg:any;
  failedMsg:any;

  pokemonDetailForm = new FormGroup({
    'pokemonId': new FormControl('', Validators.required),
    'pokemonName': new FormControl('', Validators.required),
    'pokemonSpecies': new FormControl('', Validators.required),
    'pokemonMove': new FormControl('', Validators.required),
    'pokemonType': new FormControl('', Validators.required),
    'owned': new FormControl('',Validators.required),
    'ownedAmount': new FormControl('',Validators.required)
    // 'pokemonPicture':new FormControl('')
  })

  ngOnInit(): void {
    this.getParamId = this.router.snapshot.paramMap.get('id')
    this.service.getOneData(this.getParamId).subscribe((res) => {
      this.pokemonDetailForm.patchValue({
        pokemonId:res.id,
        pokemonName:res.name,
        pokemonSpecies:res.species.name,
        pokemonMove:res.moves[0].move.name,
        pokemonType:res.types[0].type.name,
        owned:res.owned,
        ownedAmount:res.ownedAmount
      })
    })
  }

  refresh(): void {
    window.location.reload();
  }

  catchOnePokemon(){
    this.service.catchOnePOkemon(this.getParamId).subscribe((res)=>{
      this.refresh();
      if(res.succeed){
        // setTimeout(()=>{},10000)
        this.successMsg = res.message;
      }else{
        // setTimeout(()=>{this.refresh()},10000)
        this.failedMsg = res.message;
      }
    })
  }

  releaseOnePokemon(){
    this.service.releaseOnePOkemon(this.getParamId).subscribe((res)=>{
      this.refresh()
      if(res.succeed){
        // setTimeout(()=>{},4000)
        this.successMsg = res.message;
      }else{
        // setTimeout(()=>{},4000)
        this.failedMsg = res.message;
      }
    })
  }

  renameOnePokemon(){
    this.service.renameOnePOkemon(this.getParamId).subscribe((res)=>{
      this.refresh()
      if(res.succeed){
        // setTimeout(()=>{},4000)
        this.successMsg = res.message;
      }else{
        // setTimeout(()=>{},4000)
        this.failedMsg = res.message;
      }
    })
  }
}
