import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PokemonApiService } from '../pokemon-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-add',
  templateUrl: './pokemon-add.component.html',
  styleUrls: ['./pokemon-add.component.css']
})
export class PokemonAddComponent implements OnInit {

  constructor(private service: PokemonApiService,private router:Router) { }

  errormsg: any;
  successmsg:any;
  selectedPicture:any;

  ngOnInit(): void {
  }

  pokemonForm = new FormGroup({
    'pokemonId': new FormControl('', Validators.required),
    'pokemonName': new FormControl('', Validators.required),
    'pokemonSpecies': new FormControl('', Validators.required),
    'pokemonMove': new FormControl('', Validators.required),
    'pokemonType': new FormControl('', Validators.required),
    'pokemonPicture':new FormControl('', Validators.required)
  })

  refresh(): void {
    window.location.reload();
  }

  selectOnPicture(event:any){
    this.selectedPicture = <File>event.target.files[0]
  }

  pokemonSubmit() {
    if (this.pokemonForm.valid) {
      const newPokemon = {
        id:this.pokemonForm.value.pokemonId,
        name:this.pokemonForm.value.pokemonName,
        species:{
          name:this.pokemonForm.value.pokemonSpecies,
          url:"https://pokeapi.co/api/v2/pokemon-species/"+this.pokemonForm.value.pokemonSpecies
        },
        moves:[{
          move:{
            name:this.pokemonForm.value.pokemonMove,
            url:"https://pokeapi.co/api/v2/move/"+this.pokemonForm.value.pokemonMove
          }
        }],
        types:[{
          slot:1,
          type:{
            name:this.pokemonForm.value.pokemonType,
            url:"https://pokeapi.co/api/v2/type/"+this.pokemonForm.value.pokemonType
          }
        }],
        picture:{
          name:this.selectedPicture.name,
          url:"./assets/" + this.selectedPicture.name
        }
      }
      const fd = new FormData();
      fd.append('image', this.selectedPicture, this.selectedPicture.name)


      this.service.createData(newPokemon).subscribe((res)=>{
        console.log(res,'res==>')
      })
      console.log(this.selectedPicture)
      this.service.uploadFile(fd).subscribe((res)=>{
        console.log(this.selectedPicture)
        console.log(res,'res==>')
      })
      this.successmsg = 'New Pokemon has been created'
      this.refresh()
      this.router.navigate(['/list'])
      this.router.navigate(['/list'])
    } else {
      setTimeout(()=>{},4000)
      this.errormsg = 'All fields are required!'
    }
  }

}
