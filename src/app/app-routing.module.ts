import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonAddComponent } from './pokemon-add/pokemon-add.component';
import { PokemonDetailComponent } from './pokemon-detail/pokemon-detail.component';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PokemonOwnedComponent } from './pokemon-owned/pokemon-owned.component';

const routes: Routes = [
  {path:'list',component:PokemonListComponent},
  {path:'detail/:id',component:PokemonDetailComponent},
  {path:'add',component:PokemonAddComponent},
  {path:'ownedList',component:PokemonOwnedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
