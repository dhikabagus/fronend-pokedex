import { Component, OnInit, ɵɵqueryRefresh} from '@angular/core';
import { PokemonApiService } from '../pokemon-api.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css'],
})

export class PokemonListComponent implements OnInit {

  constructor(private service: PokemonApiService) { }
  pokemonDatas: any;
  urls = ["./assets/pikachu.png",
          "./assets/bulbasaur.png"]


  ngOnInit(): void {
    this.service.getAllData().subscribe((res) => {
      this.pokemonDatas = res.data;
      console.log(this.pokemonDatas)
    })
  }
  refresh(): void {
    window.location.reload();
  }
  //Delete Pokemon Data
  deletePokemon(pokemonId: any) {
    this.service.deleteData(pokemonId).subscribe()
    this.refresh()
  }
  ownedOrNot(owned: Boolean) {
    return owned ? 'YES' : 'NO'
  }
}
