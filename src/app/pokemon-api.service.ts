import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {

  constructor(private _http:HttpClient) { }

  //Connect frontend to backend
  apiUrl = 'http://localhost:3000/api/v1/pokemons'

  //*GET pokemons data*//
  getAllData():Observable<any>{
    return this._http.get(`${this.apiUrl}`);
  }

  getAllOwnedData():Observable<any>{
    return this._http.get(`${this.apiUrl}/mypokemon/owned`)
  }

  getOneData(pokemonId:any):Observable<any>{
    return this._http.get(`${this.apiUrl}/${pokemonId}`)
  }

  //*CREATE new pokemon data*//
  createData(data:any):Observable<any>{
    console.log(data, 'createapi==>')
    return this._http.post(`${this.apiUrl}`,data)
  }

  //*DELETE a pokemon data*//
  deleteData(pokemonId:Number):Observable<any>{
    return this._http.delete(`${this.apiUrl}/${pokemonId}`)
  }

  catchOnePOkemon(pokemonId:Number):Observable<any>{
    return this._http.get(`${this.apiUrl}/catch/${pokemonId}`)
  }

  releaseOnePOkemon(pokemonId:Number):Observable<any>{
    return this._http.get(`${this.apiUrl}/release/${pokemonId}`)
  }

  renameOnePOkemon(pokemonId:Number):Observable<any>{
    return this._http.get(`${this.apiUrl}/rename/${pokemonId}`)
  }

  uploadFile(file:FormData):Observable<any>{
    return this._http.post(`${this.apiUrl}/uploadFile`,file)
  }
}
